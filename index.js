import { AppRegistry } from 'react-native';
import ReactDOM from "react-dom";
import React from 'react';
import App from './App';
import {Provider} from "react-redux";
import {applyMiddleware, compose,  createStore} from "redux";
import thunkMiddleware from "redux-thunk";
import reducer from "./src/store/reduers/reducer";
import thunk from "redux-thunk";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(reducer, composeEnhancers(applyMiddleware(thunk)));

const Application = () => (
    <Provider store={store}>
        <App />
    </Provider>
);
AppRegistry.registerComponent('mobile', () => Application);
