import * as actionTypes from "../actions/action";
const initialState = {

    contacts: {},
    request: false,
    error: false,
    modal: false,
    obj: {}


};

const reducer = (state = initialState, action) => {
    console.log("REDUCER", action);
    switch (action.type) {
        case actionTypes.DATA_REQUEST:
            return {...state, contacts: {...state.contacts}, request: true, error: false, editor: false};
        case actionTypes.DATA_SUCCESS: {
            console.log("Success")
            return {...state, contacts: action.data, editor: false};

        }

        case actionTypes.DATA_ERROR:
            return {...state, contacts: {...state.contacts}, request: false, error: true, editor: false};
        case actionTypes.SHOW_MODAL: {
            console.log('CLIIIIIICK', action.elem);
            return {...state, modal: true, obj: action.elem};
        }
        case  actionTypes.HIDE_MODAL: {
            return {...state, modal: false}
        }


        default: {
            return state;
            console.log("DEFFALT")
        }
    }
};
export default reducer;