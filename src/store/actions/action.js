import axios from "../../axios-orders";

export const DATA_REQUEST = 'DATA_REQUEST';
export const DATA_SUCCESS = 'DATA_SUCCESS';
export const DATA_SUCCESS_ADD = 'DATA_SUCCESS_ADD';
export const DATA_ERROR = 'DATA_ERROR';
export const SHOW_MODAL = 'SHOW_MODAL';
export const HIDE_MODAL = 'HIDE_MODAL';


export const dataRequest = () => {
    return {type: DATA_REQUEST};
    console.log("REQUEST")
};
export const dataSuccess = (data) => {
    return {type: DATA_SUCCESS, data}
};
export const dataSuccessAdd = () => {
    return {type: DATA_SUCCESS_ADD}
};
export const dataError = () => {
    return {type: DATA_ERROR}
};
export const getContacts = () => {
    console.log("ENTER_ACTION");

    return (dispatch, getState) => {
        dispatch(dataRequest());
        axios.interceptors.request.use(request => {
            console.log('request', request);
            return request;
        });
        axios.get('/contacts.json').then(response => {
            dispatch(dataSuccess(response.data));
            console.log("THEN")

        }, error => {
            console.log('error', error);
            dispatch(dataError())
        })
    }
};
export const showContact = (elem) => {


    return {type: SHOW_MODAL, elem}
};
export const hideContact = () => {


    return {type: HIDE_MODAL}
};



