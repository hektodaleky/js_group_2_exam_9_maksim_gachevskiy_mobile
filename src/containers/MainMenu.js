import React, {Component} from "react";
import Contact from "../component/Item/Contact";
import {FlatList, Item, Modal, View} from "react-native";
import {connect} from "react-redux";
import {getContacts, hideContact, showContact} from "../store/actions/action";
import InfoModal from "../component/InfoModal/InfoModal";
class MainMenu extends Component {

    componentDidMount() {
        this.props.getContacts();
    }

    render() {
        let allCont = [];
        Object.keys(this.props.contacts).map((elem, i) => {
            let oneItem = {...this.props.contacts[elem], id: elem};
            allCont.push(
                {
                    key: oneItem.id, imgLink: {uri: oneItem.photo},
                    contName: oneItem.name, pressed: () => this.props.showContact(oneItem)
                }
            )

        });

        return (


            <View>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.props.modal}
                    onRequestClose={() => {
                        this.props.hideContact();
                    }}>
                    <InfoModal/>

                </Modal>

                <FlatList
                    data={allCont}
                    renderItem={(info) => {
                        return <Contact
                            imgLink={info.item.imgLink}
                            contName={info.item.contName}
                            pressed={info.item.pressed}/>
                    }}/>


            </View>)

    }
}

const mapStateToProps = state => {
    return {
        contacts: state.contacts,
        modal: state.modal
    }
};

const mapDispatchToProps = dispatch => {
    return {
        getContacts: () => dispatch(getContacts()),
        showContact: (elem) => dispatch(showContact(elem)),
        hideContact:()=>dispatch(hideContact())

    };
};
export default connect(mapStateToProps, mapDispatchToProps)(MainMenu);