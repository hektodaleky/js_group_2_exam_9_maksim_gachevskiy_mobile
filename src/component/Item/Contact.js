import React from "react";
import {Image, StyleSheet, Text, Touchable, TouchableOpacity} from "react-native";

const Item = props => {
        return (
            <TouchableOpacity onPress={props.pressed}
                              style={style.item}>
                <Image resizeMode="contain"
                       source={props.imgLink}
                       style={style.image}/>
                <Text style={style.text}>{props.contName}</Text>
            </TouchableOpacity>);
    }
;
const style = StyleSheet.create({
    item: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        backgroundColor: '#ccc',
        marginBottom: 10,
        padding: 10
    },
    image: {
        width: 50,
        height: 50,
        marginRight: 10,


    },
    text:{
       width:'80%'
    }

});

export default Item;
