import React, {Component} from "react";
import Item from "../Item/Contact";
import {connect} from "react-redux";
import {Button, Image, StyleSheet, Text,View} from "react-native";
import {hideContact} from "../../store/actions/action";
class InfoModal extends Component {


    render() {
        console.log(this.props.obj);
        return (

            <View style={style.item}>
                <Image resizeMode="contain"
                source={{uri:this.props.obj.photo}}
                style={style.image}/>
                <Text style={style.text}>{this.props.obj.name}</Text>
                <Text style={style.text}>{this.props.obj.phone}</Text>
                <Text style={style.text}>{this.props.obj.email}</Text>
                <Button title="Back" onPress={this.props.hideContact}/>



            </View>)
    }
}

const mapStateToProps = state => {
    return {
        obj: state.obj
    }
};

const mapDispatchToProps = dispatch => {
    return {
        hideContact:()=>dispatch(hideContact())
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(InfoModal);

const style = StyleSheet.create({
    item: {

    },
    image: {

width:100,
        height:100

    },
    text: {

        fontSize: 30
    }

});